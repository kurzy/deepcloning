// https://github.com/mayank7924/JS-snippets/blob/main/createDeepCopy.js

const deepCopy = (object) => {
  if (object instanceof Date) {
    return new Date(object.getTime());
  }

  if (typeof object !== "object") {
    return object;
  }

  const copy = Array.isArray(object) ? [] : {};

  for (const key in object) {
    const value = object[key];
    copy[key] = deepCopy(value);
  }

  return copy;
};

/* #region  Mám knihkupectví */
const knihy = [ // budu robit změny v týhle knížce, jo…
  {
    autor: 'George Orwell',
    nazev: '1984',
    vydani: [
      { datum: new Date(1984, 1, 1), vydavatel: 'Index' },
      { datum: new Date(1991, 1, 1), vydavatel: ' Naše vojsko' },
      { datum: new Date(1998, 1, 1), vydavatel: 'Knižná dielňa Timotej' },
    ],
  },
  {
    autor: 'Jack London',
    nazev: 'Volání divočiny',
    vydani: [
      { datum: new Date(1918, 1, 1), vydavatel: 'B. Kočí' },
      { datum: new Date(1937, 1, 1), vydavatel: 'Tisk Zlín' },
      { datum: new Date(1951, 1, 1), vydavatel: 'Práce' },
    ],
  },
  {
    autor: 'Joy Adamson',
    nazev: 'Volání divočiny',
    vydani: [
      { datum: new Date(1984, 1, 1), vydavatel: 'Panorama' },
    ],
  },
  {
    autor: 'Jules Verne',
    nazev: 'Dvacet tisíc mil pod mořem',
    vydani: [
      { datum: new Date(1907, 1, 1), vydavatel: 'B. Kočí' },
      { datum: new Date(1913, 1, 1), vydavatel: 'E. Beaufort' },
      { datum: new Date(1926, 1, 1), vydavatel: 'Josef R. Vilímek' },
    ],
  },
  {
    autor: 'Alexandre Dumas ml.',
    nazev: 'Dáma s kaméliemi',
    vydani: [
      { datum: new Date(1893, 1, 1), vydavatel: 'A. Storch a syn' },
      { datum: new Date(1919, 1, 1), vydavatel: 'František Bačkovský' },
      { datum: new Date(1932, 1, 1), vydavatel: 'Jan Toužimský' },
    ],
  },
]
/* #endregion */

/* #region  Koupím si všechny knihy */
const mojeKnihy = deepCopy(knihy);
knihy[0].vydani.push({ datum: new Date(2000, 1, 1), vydavatel: 'Levné knihy' });
/* #endregion */

/* #region  Ale moje knihy furt při starém */
console.log(knihy[0].vydani.length);
console.log(mojeKnihy[0].vydani.length);
console.log(mojeKnihy[0].vydani[0].datum.getFullYear());
/* #endregion */

/* #region  Unikátní názvy knih */
const unikaty = new Set(mojeKnihy.map((kniha) => kniha.nazev));
console.log(unikaty);
/* #endregion */

/* #region  Co nebudu půjčovat */
const nepujcovat = deepCopy(unikaty);
console.log(nepujcovat);
/* #endregion */

/* #region  Nově příchozí do třídy */
class Clovek {
  #vek;
  #jmeno;

  constructor(jmeno, vek) {
    this.#jmeno = jmeno;
    this.#vek = vek;
  }

  get vek() {
    return this.#vek;
  }

  kdoJsem() {
    return `Jmenuji se ${this.#jmeno} a je mi ${this.#vek} let.`;
  }
}

const prichozi = new Clovek('Vilda', 80);
console.log(prichozi.vek);
console.log(prichozi.kdoJsem());
/* #endregion */

/* #region  Je to příchodník */
const vejda = deepCopy(prichozi);
console.log(vejda.vek);
console.log(vejda.kdoJsem);
/* #endregion */
