const pruvodce = ['kniha', 'Don\'t panic', 42, 'Hlubina myšlení', 'ručník'];

/* #region  Udělám kopii */
const falzifikat = [...pruvodce];
/* #endregion */

/* #region  Kdo je hlavní postava? */
pruvodce.push('Arthur Dent');
/* #endregion */

/* #region  A co na to naše falzum? */
console.log(pruvodce);
console.log(falzifikat);
/* #endregion */

/* #region  Chcete zpátky do cely, vaše výsosti Hane Solo? */
const hrdina = { jmeno: 'Han', prijmeni: 'Solo', rasa: 'člověk', stav: 'živý' };
/* #endregion */

/* #region  Po jedné návštěvě se mu něco přihodilo */
// const zmrzlik = Object.assign({}, hrdina, {stav: 'zmrzlý'});
const zmrzlik = { ...hrdina, stav: 'zmrzlý' };
/* #endregion */

/* #region  A pak se zamiloval 💖💓💞💕💘 */
hrdina.partnerka = 'Leia Organa';
/* #endregion */

/* #region  Kdopak měl smůlu? */
console.log(hrdina);
console.log(zmrzlik);
/* #endregion */

/* #region  Ovšem, je to celé takové povrchní */
const pythoni = [
  { jmeno: 'Graham', prijmeni: 'Chapman', narozen: '8. leden 1941' },
  { jmeno: 'John', prijmeni: 'Cleese', narozen: '27. října 1939' },
  { jmeno: 'Terry', prijmeni: 'Gilliam', narozen: '	22. listopad 1940' },
  { jmeno: 'Eric', prijmeni: 'Idle', narozen: '29. březen 1943' },
  { jmeno: 'Terry', prijmeni: 'Jones', narozen: '1. únor 1942' },
  { jmeno: 'Michael', prijmeni: 'Palin', narozen: '5. květen 1943' },
];
/* #endregion */

/* #region  Uděláme kopii */
const montni = [...pythoni];
// const monthni = Object.assign([], pythoni);
/* #endregion */

/* #region  Trocha smutku 😢 */
pythoni[0].umrti = '4. říjen 1989';
pythoni[4].umrti = '21. leden 2020';
/* #endregion */

/* #region  Aktuální stav u Montnů */
console.log(montni[0].umrti);
/* #endregion */
