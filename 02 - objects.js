const poleOriginal = [1, 2, 3, 4, 5];
const poleKopie = poleOriginal;

console.log(poleOriginal, poleKopie);

poleOriginal.push(6);

/* #region  Mám kopii? */
console.log(poleOriginal, poleKopie);
/* #endregion */

/* #region  Vyrobím auto */
const prvniAuto = {
  znacka: 'Škoda Favorit',
  barva: '#FFFFFF',
  dejZvuk: () => 'brum prásk',
};

const druheAuto = prvniAuto;
/* #endregion */

/* #region  Mám stejná auta? */
console.log(prvniAuto === druheAuto);
/* #endregion */

/* #region  První auto je luxusní */
prvniAuto.znacka = 'Mercedes';
/* #endregion */

/* #region  Mám pořád stejná auta? */
console.log(prvniAuto === druheAuto);
/* #endregion */

/* #region  Jaké je druhé auto? */
console.log(druheAuto);
/* #endregion */
