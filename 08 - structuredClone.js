const vecernicky = new Map();

vecernicky.set('O loupežníku Rumcajsovi', {
  poprve: new Date(1967, 11, 5),
  partneri: new Set(['Manka', 'Cipisek', 'Česílko', 'Volšoveček', 'sojka']),
  protivnici: new Set(['knížepán', 'kněžna', 'Fricek', 'Humpál', 'Kule']),
  autori: [
    { typ: 'text', jmeno: 'Václav', prijmeni: 'Čtvrtek', narozen: new Date(1911, 4, 4) },
    { typ: 'kresba', jmeno: 'Radek', prijmeni: 'Pilař', narozen: new Date(1931, 4, 23) },
    { typ: 'namluvil', jmeno: 'Karel', prijmeni: 'Höger', narozen: new Date(1909, 6, 17) },
  ],
});

vecernicky.set('O krtkovi', {
  poprve: new Date(1979, 4, 28),
  partneri: new Set(['čáp', 'len', 'ježek', 'mravenci', 'rak', 'rákosník']),
  protivnici: new Set(['kluk', 'straka', 'kocour', 'zahradník', 'prodavač', 'lišák']),
  autori: [
    { typ: 'text a kresba', jmeno: 'Zdeněk', prijmeni: 'Müller', narozen: new Date(1921, 2, 21) },
    { typ: 'zvuky', jmeno: 'Strýček', prijmeni: 'Jedlička', narozen: new Date(1923, 2, 18) },
  ],
})

/* #region  Made in PRC */
const cinskeVecernicky = structuredClone(vecernicky);
/* #endregion */

/* #region  Hudbu složil */
vecernicky.get('O krtkovi')
  .autori
  .push({ typ: 'hudba', jmeno: 'Vadim', prijmeni: 'Petrov', narozen: new Date(1932, 5, 24) });
/* #endregion */

/* #region  Originál vs. Aliexpress */
console.log(vecernicky.get('O krtkovi').autori.length);
console.log(cinskeVecernicky.get('O krtkovi').autori.length);
/* #endregion */

/* #region  Oni jsou sice šikovní, ale ne všechno jde */
// Chyba
// https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Structured_clone_algorithm#supported_types
/*
const cinskyKrtek = cinskeVecernicky.get('O krtkovi');
cinskyKrtek.promluv = () => '鼹鼠的故事';
const mluviciKrtek = structuredClone(cinskyKrtek);
*/

/* #endregion */

/* #region  Transfer objektů */
// https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Transferable_objects
const buffik = new ArrayBuffer(16);
const object = { buffer: buffik };

console.log(object.buffer.byteLength)

const objectCopy = structuredClone(
  object,
  { transfer: [buffik] },
);

console.log(object.buffer.byteLength);
console.log(objectCopy.buffer.byteLength);
/* #endregion */
