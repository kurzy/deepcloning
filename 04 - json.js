const vojaci = [
  { jmeno: 'Fett', typ: 'originál', oznaceni: undefined },
  { jmeno: 'Rex', typ: 'klon', oznaceni: 'CT-7567' },
  { jmeno: 'Tup', typ: 'klon', oznaceni: 'CT-5385' },
  { jmeno: 'Gree', typ: 'klon', oznaceni: 'CC-1004' },
];

/* #region  Naklonuju vojáky */
const klony = JSON.parse(JSON.stringify(vojaci));
/* #endregion */

/* #region  Přidám nového komandéra */
vojaci.push({ jmeno: 'Wolffe', typ: 'klon', oznaceni: 'CC-3636' });
/* #endregion */

/* #region  Kdepak ho všude najdu? */
console.log(vojaci.length);
console.log(klony.length);
/* #endregion */

/* #region  Příklad z restaurace */
const stopar = {
  jmeno: 'Arthur',
  prijmeni: 'Dent',
  obleceni: 'župan',
  vybava: ['taška'],
};

const kopieArthura = JSON.parse(JSON.stringify(stopar));
/* #endregion */

/* #region  Základní výbava každého stopaře je… */
stopar.vybava.push('ručnik');
/* #endregion */

/* #region  Ale má ji i Arthurova kopie? */
console.log(stopar.vybava);
console.log(kopieArthura.vybava);
/* #endregion */

/* #region  Tuhle odpověď zná každý */
stopar.odpoved = () => 42;
console.log(stopar.odpoved());
/* #endregion */

/* #region  První kopie 👎 */
const lepsiKopieArthura = JSON.parse(JSON.stringify(stopar));
/* #endregion */

/* #region  A jak to nakonec celé dopadlo? */
console.log(lepsiKopieArthura);
console.log(lepsiKopieArthura.odpoved);
/* #endregion */

/* #region  Kluci maj narozeniny, my máme přání jediný */
const narozeniny = {
  adams: new Date(1952, 3, 11),
  lucas: new Date(1944, 5, 14),
  cleese: new Date(1939, 10, 27),
};

console.log(narozeniny.lucas.getDate());
console.log(narozeniny.adams.constructor.name);
/* #endregion */

/* #region  Těžce sehnaná data nedám z ruky, udělám kopii */
const narozky = JSON.parse(JSON.stringify(narozeniny));
/* #endregion */

/* #region  Ale božíčku! */
console.log(typeof narozky.cleese);
console.log(narozky.cleese.constructor.name);
/* #endregion */
