# DeepCloning v Javascriptu

## Základ přednášky

Vyvořeno na základě [článku Steveho Sewella](https://www.builder.io/blog/structured-clone) publikovaného 18. ledna. 2023
## Osnova
- porovnání primitivních typů
- porovnání polí a objektů
- klonování HTML elementů
- klonování přes JSON stringifikací a parsováním
- klonování přes destukturalizaci
- vlastní funkce
- klonování přes Lodash cloneDeep
- finále: structuredClone
