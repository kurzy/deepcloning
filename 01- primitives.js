let a = 1;
const b = a;

console.log(a, b);

a += 41;

/* #region  Jak to vypadá? */
console.log(a, b);
/* #endregion */

/* #region  Jedi */
let jmenoMoje = 'Anakin';
const jmenoJediho = jmenoMoje;

console.log(jmenoMoje, jmenoJediho);
/* #endregion */

/* #region  Po proměně */
jmenoMoje = 'Darth Vader';

console.log(jmenoMoje, jmenoJediho)
/* #endregion */
