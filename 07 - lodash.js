const cloneDeep = require('lodash.clonedeep');

const dnes = {
  datum: new Date(2023, 3, 8),
  nalada: '😁🐱‍🏍🐱‍🚀',
  drinky: new Set(['kafe', 'čikuláda', 'pivo', 'kafe', 'Jim Beam', 'pivo', 'pivo', 'Jim Beam']),
  popis: 'taková normální středa…',
  vyznamneOkamziky: [
    { nazev: 'probudil jsem se', okamzik: (new Map()).set('h', 7).set('m', 14) },
    { nazev: 'prvni pivo', okamzik: (new Map()).set('h', 7).set('m', 15) },
    { nazev: 'příchod do práce', okamzik: (new Map()).set('h', 8).set('m', 25) },
    { nazev: 'odchod na Frontendisty', okamzik: (new Map()).set('h', 15).set('m', 31) },
    { nazev: 'návštěva pohostinství', okamzik: (new Map()).set('h', 16).set('m', 4) },
    { nazev: 'pžiíichood k Frronteenditsum', okamzik: (new Map()).set('h', 17).set('m', 2) },
    { nazev: 'oodjzszsd do blaavy', okamzik: (new Map()).set('h', 20).set('m', 22) },
  ],
};

/* #region  S drobnými odchylkami i za měsíc */
const coBudeZaMěsíc = cloneDeep(dnes);
coBudeZaMěsíc.datum.setMonth(3);
coBudeZaMěsíc.datum.setDate(12);
coBudeZaMěsíc.drinky.add('zelená');
coBudeZaMěsíc.vyznamneOkamziky[6].okamzik.set('h', 20).set('m', 24);
/* #endregion */

/* #region  Jak to vypadá dnes? */
console.log(dnes.datum);
console.log(dnes.drinky);
console.log(dnes.vyznamneOkamziky[6].okamzik);
/* #endregion */

/* #region  A jak to bude vypadat za měsíc? */
console.log(coBudeZaMěsíc.datum);
console.log(coBudeZaMěsíc.drinky);
console.log(coBudeZaMěsíc.vyznamneOkamziky[6].okamzik);
/* #endregion */
