const jsdom = require('jsdom');
const { JSDOM } = jsdom;

const { window: { document } } = new JSDOM();

const bold = document.createElement('strong');
const paragraph = document.createElement('p');
paragraph.className = 'enemy';

bold.textContent = 'otec';

paragraph.textContent = 'Já jsem tvůj ';
paragraph.appendChild(bold);
paragraph.appendChild(document.createTextNode(', Luku!'));

/* #region  Mladý Darth Vader */
const youngDarthVader = paragraph.cloneNode();
/* #endregion */

/* #region  Jak vypadá */
console.log(youngDarthVader.textContent);
console.log(youngDarthVader.className);
/* #endregion */

/* #region  Pokročíme v čase a hloubce */
const oldDarthVader = paragraph.cloneNode(true);
/* #endregion */

/* #region  Jak vypadá D. V. včil? */
console.log(oldDarthVader.textContent);
console.log(oldDarthVader.className);
/* #endregion */


/* #region  D. V. má rád své děti */
paragraph.className = 'my-father';
paragraph.textContent = 'Pocem, ty můj kluku!';

/* #endregion */

/* #region  A jak to celé dopadlo? */
console.log(paragraph.textContent);
console.log(paragraph.className);
console.log(youngDarthVader.textContent);
console.log(youngDarthVader.className);
console.log(oldDarthVader.textContent);
console.log(oldDarthVader.className);
/* #endregion */
